import React, {Component} from 'react';
import Task from './component/Task/Task';
import AddTaskForm from './component/AddTaskForm/AddTaskForm';
import { Container, Col } from 'reactstrap';

class App extends Component {
    state = {
        tasks: [
            {id: 1, name: 'task #1'},
            {id: 2, name: 'task #2'},
            {id: 3, name: 'task #3'},
            {id: 4, name: 'task #4'},
            {id: 5, name: 'task #5'}
        ],
        taskText: '',
        currentId: 5
    };

    changeHandler = (event) => {
        this.setState({
            taskText: event.target.value
        })
    };

    addNewTask = (event) => {
        event.preventDefault();

        let arrTasks = [...this.state.tasks];

        let newTask = {
            name: this.state.taskText,
            currentId: this.state.currentId + 1
        };

        arrTasks.push(newTask);

        this.setState({
            tasks: arrTasks,
            currentId: this.state.currentId + 1
        })

    };

    deleteTask = id => {

        let tasks = [...this.state.tasks];
        let searchId = tasks.findIndex(task => {
            return task.id === id;
        });

        tasks.splice(searchId, 1);

        this.setState({
            tasks: tasks,
            currentId: this.state.currentId - 1
        });
    };

    render() {
        return (
            <Container>
                <Col sm={{ size: 6, offset: 3 }}>
                    <AddTaskForm
                        addNewTask={(event) => this.addNewTask(event)}
                        changeInput={(event) => this.changeHandler(event)}
                    />
                </Col>
                <Col sm={{ size: 10, offset: 1 }}>
                    <ul className="all-tasks-list">
                        <Task
                            allTasks={this.state.tasks}
                            remove={(id) => this.deleteTask(id)}
                        />
                    </ul>
                </Col>
            </Container>
        );
    }
}

export default App;
