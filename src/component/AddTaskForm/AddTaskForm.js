import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';


const AddTaskForm = (props) => {

    return (
        <Form>
            <FormGroup>
                <Label for="AddTaskInput">Add task form!</Label>
                <Input onChange={props.changeInput} type="text" name="task" id="AddTaskInput" placeholder="Add your task here..." />
            </FormGroup>
            <Button onClick={props.addNewTask} color="success">Add</Button>
        </Form>
    );
};

export default AddTaskForm;
