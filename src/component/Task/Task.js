import React from 'react';
import {Button} from 'reactstrap';

const Task = props => {
    return (
        props.allTasks.map(task => {
            return (
                <li key={task.id}>
                    <p>{task.name}</p>
                    <Button onClick={() => props.remove(task.id)} color="danger">Delete</Button>
                </li>
            )
        })
    );
};

export default Task;
